// Initialise latitude et longitude (centre de la carte)
var lat = 46.804300;
var lon = 1.443700;
var macarte = null;

// Fonction d'initialisation de la carte
function initMap() {
	// Créer l'objet "macarte" insèrer dans élément HTML ID "map"
    macarte = L.map('map').setView([lat, lon], 6);
    L.tileLayer('https://{s}.tile.openstreetmap.fr/osmfr/{z}/{x}/{y}.png', {
        attribution: 'données © <a href="//osm.org/copyright">OpenStreetMap</a>/ODbL - rendu <a href="//openstreetmap.fr">OSM France</a>',
        minZoom: 1,
        maxZoom: 20
    }).addTo(macarte);
	for (ville in villes) {
		var marker = L.marker([villes[ville].lat, villes[ville].lon]).addTo(macarte);
		marker.bindPopup(ville).openPopup();
	}  
}
window.onload = function(){
	initMap(); 
};
