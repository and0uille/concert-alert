CREATE TABLE IF NOT EXISTS coordinates (
    city TEXT,
    lon FLOAT,
    lat FLOAT
);

CREATE TABLE IF NOT EXISTS concerts (
    hash VARCHAR(32) PRIMARY KEY, 
    epoch INTEGER
);
