<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:template match="/root">
<html>
	<head>
		<link type="text/css" rel="stylesheet" href="index.css" />
		<link rel="stylesheet" href="https://unpkg.com/leaflet@1.3.1/dist/leaflet.css" integrity="sha512-Rksm5RenBEKSKFjgI3a41vrjkw4EVPlJ3+OiI65vTjIdo9brlAacEuKOiQ5OFh7cOI1bkDwLqdLw3Zg0cRJAAQ==" crossorigin="" />
		<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css"/>
		<link rel="stylesheet" href="/resources/demos/style.css"/>
		<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
		<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
		<script>
		  $( function() {
			$( "#tabs" ).tabs();
		  } );
		</script>
		<script src="https://unpkg.com/leaflet@1.3.1/dist/leaflet.js" integrity="sha512-/Nsx9X4HebavoBvEBuyp3I7od5tA0UzAxs+j83KgC8PU0kgB4XiK4Lfe4y4cgBtaRJQEIFCW+oC506aPT2L1zw==" crossorigin=""></script>
		<script type="text/javascript" src="scripts/coord.js"></script>
		<script type="text/javascript" src="scripts/osm.js"></script>
	</head>
	<body>
		<header>
			<h1>Cartoconcert</h1>
			<br></br>
			<h3><xsl:value-of select="title"/></h3>
		</header>
		<div id="tabs">
		<div id="nav-center">
			<ul id="horizontal-list">
				<li><a href="#tabs-1">Carte</a></li>
				<li><a href="#tabs-2">Liste</a></li>
				<li><a href="#tabs-3">Nouveaux</a></li>
			</ul>
		</div>
		<br></br>
			<div id="tabs-1">
				<hr color="black" size="5"></hr> 
				<br></br>
				<div id="map">
					<!-- Ici s'affichera la carte -->
				</div>
				<br></br>
				<hr color="black" size="5"></hr> 
				<br></br>
			</div>
			<div id="tabs-2">
				<hr color="black" size="5"></hr> 
				<br></br>
				<table class="concerts">
					<tr>
						<th>Groupe</th>
						<th>Date</th>
						<th>Ville</th>
						<th>Dpt</th>
					</tr>
					<xsl:for-each select="concerts/band">
					<xsl:for-each select="concert">
					<tr>
						<xsl:choose>
						<xsl:when test="position() = 1">
							<td class="band" rowspan="{last()}">
								<xsl:value-of select="ancestor::band[1]/bandname"/>
							</td>
							<td class="first"><xsl:value-of select="date"/></td>
							<td class="first"><xsl:value-of select="city"/></td>
							<td class="first"><xsl:value-of select="dpt"/></td>
						</xsl:when>
						<xsl:otherwise>
							<td><xsl:value-of select="date"/></td>
							<td><xsl:value-of select="city"/></td>
							<td><xsl:value-of select="dpt"/></td>
						</xsl:otherwise>
						</xsl:choose>
					</tr>
					</xsl:for-each>
					</xsl:for-each>
				</table>
			</div>
			<div id="tabs-3">
				<hr color="black" size="5"></hr> 
				<br></br>
				<table class="concerts">
					<tr>
						<th>Groupe</th>
						<th>Date</th>
						<th>Ville</th>
						<th>Dpt</th>
					</tr>
					<xsl:for-each select="concerts/band">
					<xsl:for-each select="concert">
					<xsl:if test="latest=1">
					<tr>
						<xsl:choose>
						<xsl:when test="position() = 1">
							<td class="band" rowspan="{last()}">
								<xsl:value-of select="ancestor::band[1]/bandname"/>
							</td>
							<td class="first"><xsl:value-of select="date"/></td>
							<td class="first"><xsl:value-of select="city"/></td>
							<td class="first"><xsl:value-of select="dpt"/></td>
						</xsl:when>
						<xsl:otherwise>
							<td><xsl:value-of select="date"/></td>
							<td><xsl:value-of select="city"/></td>
							<td><xsl:value-of select="dpt"/></td>
						</xsl:otherwise>
						</xsl:choose>
					</tr>
					</xsl:if>
					</xsl:for-each>
					</xsl:for-each>
				</table>
			</div>
		</div>
	</body>
</html>
</xsl:template>
</xsl:stylesheet>
