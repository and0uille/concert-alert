#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import urllib.request
import sys
import ssl
import readline
import gzip
import re 
import html
import argparse 
import datetime
import geocoder
import logging
import mysql.connector
import os
import hashlib
from dotenv import load_dotenv
from zipcodes import departments_dict
from lxml import etree

ssl._create_default_https_context = ssl._create_unverified_context

path = os.path.dirname(os.path.abspath(__file__))
artists_f = path+"/artists.info"
dpts_f = path+"/departements.info"
output_coord = open(path+"/scripts/coord.js", "w")
# Keys: band+location, values: coordinates
coord = {}
# Keys: coordinate, values: band+location
coord_rev = dict()

load_dotenv()
DB_PASS=os.getenv('db_password')
DB_USER=os.getenv('db_username')
DB=os.getenv('database')

mydb = mysql.connector.connect(
  host="localhost",
  user=DB_USER,
  passwd=DB_PASS,
  database=DB
)
cursor = mydb.cursor()

# Color
class Bc:
    P = '\033[95m' # purple
    B = '\033[94m' # Blue
    G = '\033[92m' # Green
    Y = '\033[93m' # Yellow
    R = '\033[91m' # Red
    ENDC = '\033[0m' # end colors

def banner():
    print(Bc.Y + "  +------------------------------------+"+ Bc.ENDC)
    print(Bc.Y + "  |            * Concerts *            |"+ Bc.ENDC)
    print(Bc.Y + "  +------------------------------------+"+ Bc.ENDC)

# URL with user defined options
def urlBuilder(band):
    return "http://www.infoconcert.com/artiste/"+band+"/concerts.html#artiste-tab-concerts"

# Gives long and lat from city name and dept
def geocoding(city, dpt):
    dpt_full = departments_dict[dpt]
    if dpt.isdigit():
        g = geocoder.osm(city+', '+dpt_full+', France')
    else:
        g = geocoder.osm(city+', '+dpt_full)

    print(g)

    # Extracting only x and y
    keys = ['x', 'y']
    dct = {key: g.osm[key] for key in keys}
    # Replace x and y with lat and lon
    dct['lat'] = dct.pop('y')
    dct['lon'] = dct.pop('x')
    return dct

def main(args):
    if args.output_type == "bash":
        banner()
    if args.output_type == "xml":
        output_xml = open(path+"/index.xml", "wb")
    if args.debug == True:
        logging.basicConfig(filename='debug.log',level=logging.DEBUG)

    # Fill HTTP headers
    headers = {
        'Host':"www.infoconcert.com",
        'User-Agent':"Mozilla/5.0 (X11; Linux x86_64; rv:51.0) Gecko/20100101 Firefox/51.0",
        'Accept':'text/javascript, text/html, application/xml, text/xml, */*',
        'Accept-Language':"en-US,en;q=0.5",
        'DNT':'1',
        'Connection':"keep-alive",
        'Referer':"www.infoconcert.com",
    }

    # Get all artists from file in list
    with open(artists_f) as f:
        bands = f.readlines()
        # Remove `\n`
        bands = [x.strip() for x in bands] 
	
    # Get all dpt from file in list
    with open(dpts_f) as f:
        dpts = f.readlines()
        # Remove `\n`
        dpts = [x.strip() for x in dpts] 
	
    # Print results by artist
    if args.output_type == "bash":
        print(Bc.R + " Resultats : " + Bc.ENDC)
    elif args.output_type == "html":
        date = datetime.datetime.now()
        with open(path+"/index.html", "w") as output_html:
            output_html.write("<h1>Update du " + date.strftime('%d') + 
                "/" + date.strftime('%m') + "/" + date.strftime("%Y") + "</h1>")
        print(Bc.R + " Resultats : " + Bc.ENDC)
    elif args.output_type == "xml":
        date = datetime.datetime.now()
        # Create XML doc and headers
        root = etree.Element("root")
        doc = etree.ElementTree(root)

        titleElt = etree.SubElement(root, 'title')
        titleElt.text = "Update du "+date.strftime('%d')+"/"+date.strftime('%m')+"/"+date.strftime("%Y")
        concertsElt = etree.SubElement(root, 'concerts')

    for band in bands:
        first_occurence = True
        url = urlBuilder(band)
        req = urllib.request.Request(url, headers=headers)
        # Request the infoconcert "API" and parse result page for each artist 
        # (return tuple <dpt, city>)
        with urllib.request.urlopen(req) as response:
            page = response.read().decode(response.headers.get_content_charset())
            if not re.findall(r"Cet artiste n'a aucun concert", page, re.S):
                # Inspect the current band concerts page, only before the 
                # archive section, which might interfere with our regexes.
                page_noarchive = page.split("Concerts passés de")[0]
                # Extract all "location blocks" (div) in the original page
                location_all = re.findall(r"<div class=\"ville-dpt\">.*?(?=</div>)", page_noarchive, re.S)
                location_filtered = []
                # For each block, check if departement matches. If it does, 
                # extract the city name
                for block in location_all:
                    for dpt in dpts:
                        if block.split()[-1] == "("+dpt+")":
                            ville = re.findall(r"<span itemprop=\"locality\">(.*?(?=</span>))</span>.*?(?=</a>)", block, re.S)
                            location_filtered.append((dpt, ville[0]))
                
                # Now that whe have a city, get the date 
                for location in location_filtered:
                    # Extract block with artist full name and date
                    bandname = band[:-5]
                    bandname = bandname.replace('-', ' ')
                    bandname = bandname.title()
                    if args.debug == True:
                        logging.debug('Band: '+bandname)
                        logging.debug('Dpt: '+location[0])
                        logging.debug('City: '+location[1])

                    # Extract date from concert item
                    # Warning: This regex is complex. Band names sometimes
                    # include unexpected characters, which makes the
                    # identification of the date difficult.
                    date = re.findall(r"\+ d'infos sur le concert de [A-Za-z0-9\-&.Ë '!/\\()+]+?(?= du) du ([0-9]{2} [A-Za-z0-9ùûé ]{5,25}) à .{2,300}?, "+location[1]+"\">", page, re.S)

                    # Reverse geocoding: Get GPS coordinates from city name
                    # and dpt. If city has never been encountered, use
                    # nominatim and save the coordinates. The value saved
                    # value will be used next time.
                    cursor.execute("SELECT * FROM coordinates WHERE city = %s", (location[1],))
                    res = cursor.fetchall()
                    coordinates = dict()
                    if len(res) == 0:
                        logging.debug('Coordinates are not in database')
                        logging.debug('Using nominatim...')
                        try:
                            coordinates = geocoding(location[1], location[0])
                        except: 
                            logging.debug("Failure to get the coordinates of " +
                                          location[1]+" " + location[0])
                            coordinates["lon"] = 0
                            coordinates["lat"] = 0

                        cursor.execute("INSERT INTO coordinates VALUES (%s, %s, %s)", 
                            (location[1], coordinates["lon"], coordinates["lat"],))
                    elif len(res) == 1:
                        city_db, lon, lat = res[0]
                        coordinates['lat'] = lat
                        coordinates['lon'] = lon
                    mydb.commit()
                    coord['<b>'+location[1]+'</b><br>'+bandname+'<br>'+date[0]] = coordinates
                    if str(coordinates) in coord_rev.keys():
                        coord_rev[str(coordinates)] += '<br>'+bandname+'<br>'+date[0]
                    else: 
                        coord_rev[str(coordinates)] = ''.join('<b>'+location[1]+'</b><br>'+bandname+'<br>'+date[0])
    	            
                    # Now that we have all the information (band name, date, and
                    # location, we can print the result in the selected format
                    if args.output_type == "bash":
                        if first_occurence is True:
                            print(Bc.G + '  [+] ' + bandname + Bc.ENDC)
                        print('     [.] Date : ' + date[0])
                        print('     [.] Ville : ' + location[1])
                        print('     [.] Departement : ' + location[0])
                        print('\n')

                        first_occurence = False
    		
                    elif args.output_type == "html":
                        item=""
                        if first_occurence is True:
                            item += "<h2>"+bandname+"</h2>"
                        item += "<ul>"
                        item += "<li> Date : "+date[0]+"</li>"
                        item += "<li> Ville : "+location[1]+"</li>"
                        item += "<li> Departement : "+location[0]+"</li>"
                        item += "</ul>"
                        first_occurence = False
                        with open(path+"/index.html", "a") as output_html:
                            output_html.write(item)

                    elif args.output_type == "xml":
                        # Adding info to XML output
                        if first_occurence is True:
                            bandElt = etree.SubElement(concertsElt, 'band')
                            bandnameElt = etree.SubElement(bandElt, 'bandname')
                            bandnameElt.text = bandname
                        concertElt = etree.SubElement(bandElt, 'concert')
                        dateElt = etree.SubElement(concertElt, 'date')
                        dateElt.text = date[0]
                        cityElt = etree.SubElement(concertElt, 'city')
                        cityElt.text = location[1]
                        dptElt = etree.SubElement(concertElt, 'dpt')
                        dptElt.text = location[0]
                        latestElt = etree.SubElement(concertElt, 'latest')
                        first_occurence = False

                        # Updating latest tables if necessary
                        m = hashlib.md5()
                        m.update(str(bandname+date[0]+location[1]).encode('utf-8'))
                        hashmd5 = m.hexdigest()
                        cursor.execute("SELECT * FROM concerts WHERE hash = %s", (hashmd5,))
                        concert_exists = cursor.fetchall()
                        epoch_time = int(datetime.datetime.now().timestamp())
                        # If concert not in db, add it
                        if len(concert_exists) == 0:
                            cursor.execute("INSERT INTO concerts VALUES (%s, %s)", (hashmd5, epoch_time,))
                            latestElt.text ="1"
                        if len(concert_exists) != 0:
                            cursor.execute("SELECT hash FROM concerts ORDER BY epoch DESC LIMIT 10")
                            latest_db = list(cursor)
                            if args.debug==True:
                                logging.debug('Already in db:')
                                logging.debug(' '+bandname)
                                logging.debug(' '+location[0])
                            if (hashmd5,) in latest_db:
                                latestElt.text = "1"
                            else:
                                latestElt.text = "0"
                        mydb.commit()
						
    if args.output_type == "xml":
        # Write XML to file
        root.addprevious(etree.PI('xml-stylesheet', 'type="text/xsl" href="index.xsl"'))
        doc.write(output_xml, xml_declaration=True, encoding='UTF-8')

        # Adjust coord dictionnay to fit js expected format
        inv_coord_rev = {v: k for k, v in coord_rev.items()}
        coordinatesWithQuotes = 'var villes = ' + repr(inv_coord_rev) + ';\n'
        coordinatesWithQuotes = coordinatesWithQuotes.replace('"{', '{')
        coordinatesWithQuotes = coordinatesWithQuotes.replace('}"', '}')
        output_coord.write(coordinatesWithQuotes)

        output_xml.close()
        output_coord.close()
        cursor.close()
        mydb.close()

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Concerts")
    parser.add_argument('--output', dest="output_type", default="bash", help="bash, html or xml")
    parser.add_argument('--debug', dest='debug', default=False, action='store_true')
    args = parser.parse_args()

    main(args)
