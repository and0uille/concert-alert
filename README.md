# Cartoconcert

Cartoconcert permet de lister et cartographier les concerts des groupes que vous pourriez aller voir.

## Installation

Cloner le déport

```bash
git clone https://framagit.org/and0uille/concert-alert.git
```

## Dépendances
* python3
* lxml==4.3.3
* geocoder==1.38.1

## Utilisation

### Renseigner les artistes à suivre et départements souhaités

Cartoconcert utilise les données fournies par infoconcert pour répertorier les données que vous souhaitez. Le artists.info doit pour l'instant être rempli de la façon suivante : 
* Se rendre sur infoconcert.com
* Rechercher un artiste
* Choisir l'artiste souhaité dans les résultats de recherches
* On atteint par exemple la page [suivante](https://www.infoconcert.com/artiste/babylon-circus-5140/concerts.html)
* Extraire la partie de l'URL contenant le nom du groupe (ex: babylon-circus-5140)
* Saisir une info de ce type par ligne dans le fichier artists.info

Le fichier departments.info contient simplement un numéro de département par ligne.

On peut obtenir la liste des concerts des artistes séléctionnés dans les département choisis sous différents formats:

### Démarrer la base de données

```bash
$ mysql -u root -p
MariaDB [(none)]> CREATE DATABASE IF NOT EXISTS cartoconcert_geo;
MariaDB [(none)]> CREATE USER cartoconcert@localhost IDENTIFIED BY "password";
MariaDB [(none)]> GRANT ALL PRIVILEGES ON cartoconcert_geo.* TO 'cartoconcert'@'localhost';
MariaDB [(none)]> flush privileges;
```

```bash
mysql -u cartoconcert -p cartoconcert_geo < create_tables.sql
```

### En ligne de commande
```bash
./concert.py --output bash
```

### En version HTML
```bash
./concert.py --output html
```
Genère un fichier index.html qui peut être directement placé à la racine d'un serveur web.

### En version XML (avec formattage et cartographie des concerts)
```bash
./concert.py --output html
```
Genère les fichiers index.xml et scripts/coord.js, qui associés au fichier de description index.xsl, à la feuille de style index.css et au script javascript scripts/osm.js permettent d'obtenir un format un peu plus joli avec un fond de carte OSM.  

![Diagramme](cartoconcert-diagram.jpg)


## License
[MIT](https://choosealicense.com/licenses/mit/)

